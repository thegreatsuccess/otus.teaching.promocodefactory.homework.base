﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.Domain.Types.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Extensions;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;


namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IEmployeeRepository employeeRepository;

        public EmployeesController(IEmployeeRepository employeeRepository)
        {
            this.employeeRepository = employeeRepository;
        }

        [HttpGet("short")]
        public async Task<List<EmployeeShortModel>> GetEmployeesAsync()
        {
            var employees = await employeeRepository.GetAllAsync().ConfigureAwait(false);
            return employees.Select<Employee, EmployeeShortModel>(x => x.ToShortModel()).ToList();
        }

        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeModel>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await employeeRepository.TryGetByIdAsync(id).ConfigureAwait(false);
            if (employee == null)
            {
                return NotFound();
            }

            return employee.ToModel();
        }

        [HttpPost]
        public async Task<ActionResult> CreateAsync([FromBody] EmployeeCreateRequestModel requestModel)
        {
            await employeeRepository.CreateAsync(requestModel.FromModel()).ConfigureAwait(false);
            return Ok();
        }

        [HttpPut]
        public async Task<ActionResult> UpdateAsync([FromBody] EmployeeUpdateRequestModel requestModel)
        {
            var employee = new Employee(
                requestModel.Id,
                requestModel.FirstName,
                requestModel.LastName,
                requestModel.Email,
                Array.Empty<Role>(),
                0);
            var result = await employeeRepository.PutAsync(employee).ConfigureAwait(false);
            if (result.Id == employee.Id)
            {
                return Created(employee.Id.ToString(), employee);
            }

            return Ok();
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            await employeeRepository.TryDeleteAsync(id).ConfigureAwait(false);
            return Ok();
        }
    }
}