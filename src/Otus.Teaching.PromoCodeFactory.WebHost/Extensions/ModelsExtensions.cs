﻿using System.Linq;
using Otus.Teaching.PromoCodeFactory.Domain.Storage.Requests;
using Otus.Teaching.PromoCodeFactory.Domain.Types.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;


namespace Otus.Teaching.PromoCodeFactory.WebHost.Extensions
{
    public static class ModelsExtensions
    {
        public static EmployeeModel ToModel(this Employee employee)
            => new()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select
                        (
                            x => new RoleModel
                            {
                                Name = x.Name,
                                Description = x.Description
                            })
                   .ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

        public static EmployeeShortModel ToShortModel(this Employee employee)
            => new()
            {
                Id = employee.Id,
                Email = employee.Email,
                FullName = employee.FullName
            };

        public static EmployeeCreateRequest FromModel(this EmployeeCreateRequestModel requestModel)
            => new(
                requestModel.FirstName,
                requestModel.LastName,
                requestModel.Email,
                requestModel.Roles.Select(x => x.FromModel()).ToList(),
                requestModel.AppliedPromocodesCount);

        public static Role FromModel(this RoleModel model) => new(model.Id, model.Name, model.Description);
    }
}