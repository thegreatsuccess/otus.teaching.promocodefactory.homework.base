﻿using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public sealed class EmployeeCreateRequestModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<RoleModel> Roles { get; set; }
        public int AppliedPromocodesCount { get; set; }
    }
}