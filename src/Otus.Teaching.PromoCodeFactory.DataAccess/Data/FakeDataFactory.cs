﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Domain.Types.Administration;


namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Employee> Employees =>
            new List<Employee>
            {
                new Employee(
                    Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                    "owner@somemail.ru",
                    "Иван",
                    "Сергеев",
                    new List<Role>
                    {
                        Roles.First(x => x.Name == "Admin")
                    },
                    5),
                new Employee(
                    Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                    "andreev@somemail.ru",
                    "Петр",
                    "Андреев",
                    new List<Role>
                    {
                        Roles.FirstOrDefault(x => x.Name == "PartnerManager")
                    },
                    10)
            };

        public static IEnumerable<Role> Roles =>
            new List<Role>
            {
                new Role(
                    Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                    "Admin",
                    "Администратор"),
                new Role(
                    Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                    "PartnerManager",
                    "Партнерский менеджер")
            };
    }
}