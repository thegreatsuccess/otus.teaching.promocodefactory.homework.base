﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core;
using Otus.Teaching.PromoCodeFactory.Core.Repositories;


namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T>
        where T : BaseEntity
    {
        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = new ConcurrentDictionary<Guid, T>(data.Select(x => new KeyValuePair<Guid, T>(x.Id, x)));
        }

        protected ConcurrentDictionary<Guid, T> Data { get; set; }

        public Task<IEnumerable<T>> GetAllAsync() => Task.FromResult((IEnumerable<T>)Data.Values);

        public Task<T?> TryGetByIdAsync(Guid id)
        {
            if (Data.TryGetValue(id, out var value))
            {
                return Task.FromResult<T?>(value);
            }

            throw new Exception("Data not found");
        }

        public async Task<T> PutAsync(T entity)
        {
            Data[entity.Id] = entity;
            return await Task.FromResult(entity).ConfigureAwait(false);
        }

        public async Task<bool> TryDeleteAsync(Guid id) =>
            await Task.FromResult(Data.TryRemove(id, out _)).ConfigureAwait(false);
    }
}