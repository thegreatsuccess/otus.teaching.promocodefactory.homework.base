﻿using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Repositories;
using Otus.Teaching.PromoCodeFactory.Domain.Storage.Requests;
using Otus.Teaching.PromoCodeFactory.Domain.Types.Administration;


namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public interface IEmployeeRepository : IRepository<Employee>
    {
        Task<Employee> CreateAsync(EmployeeCreateRequest request);
    }
}