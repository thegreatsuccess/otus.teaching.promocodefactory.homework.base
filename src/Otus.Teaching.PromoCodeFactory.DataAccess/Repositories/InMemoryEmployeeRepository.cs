﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Domain.Storage.Requests;
using Otus.Teaching.PromoCodeFactory.Domain.Types.Administration;


namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public sealed class InMemoryEmployeeRepository : InMemoryRepository<Employee>, IEmployeeRepository
    {
        public InMemoryEmployeeRepository(IEnumerable<Employee> data)
            : base(data)
        {
        }

        public async Task<Employee> CreateAsync(EmployeeCreateRequest request)
        {
            var employee = new Employee(
                Guid.NewGuid(),
                request.FirstName,
                request.LastName,
                request.Email,
                request.Roles,
                request.AppliedPromocodesCount);
            Data[employee.Id] = employee;
            return await Task.FromResult(employee).ConfigureAwait(false);
        }
    }
}