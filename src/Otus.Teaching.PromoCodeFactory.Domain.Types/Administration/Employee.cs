﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core;


namespace Otus.Teaching.PromoCodeFactory.Domain.Types.Administration
{
    public sealed class Employee : BaseEntity, IEquatable<Employee>
    {
        public Employee(
            Guid id,
            string firstName,
            string lastName,
            string email,
            IReadOnlyCollection<Role> roles,
            int appliedPromocodesCount)
            : base(id)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Roles = roles;
            AppliedPromocodesCount = appliedPromocodesCount;
        }

        public string FirstName { get; }
        public string LastName { get; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; }

        public IReadOnlyCollection<Role> Roles { get; }

        public int AppliedPromocodesCount { get; }

        public bool Equals(Employee? other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return FirstName == other.FirstName &&
                LastName == other.LastName &&
                Email == other.Email &&
                Roles.Equals(other.Roles) &&
                AppliedPromocodesCount == other.AppliedPromocodesCount;
        }

        public override bool Equals(object? obj) =>
            ReferenceEquals(this, obj) || (obj is Employee other && Equals(other));

        public override int GetHashCode() =>
            HashCode.Combine(FirstName, LastName, Email, Roles, AppliedPromocodesCount);
    }
}