﻿using System;
using Otus.Teaching.PromoCodeFactory.Core;

namespace Otus.Teaching.PromoCodeFactory.Domain.Types.Administration
{
    public class Role : BaseEntity
    {
        public Role(Guid id, string name, string description)
            : base(id)
        {
            Name = name;
            Description = description;
        }

        public string Name { get; }

        public string Description { get; }
    }
}