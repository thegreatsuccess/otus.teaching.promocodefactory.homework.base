﻿using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Domain.Types.Administration;


namespace Otus.Teaching.PromoCodeFactory.Domain.Storage.Requests
{
    public sealed class EmployeeCreateRequest
    {
        public EmployeeCreateRequest(
            string firstName,
            string lastName,
            string email,
            IReadOnlyCollection<Role> roles,
            int appliedPromocodesCount)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Roles = roles;
            AppliedPromocodesCount = appliedPromocodesCount;
        }

        public string FirstName { get; }
        public string LastName { get; }
        public string Email { get; }
        public IReadOnlyCollection<Role> Roles { get; }
        public int AppliedPromocodesCount { get; }
    }
}