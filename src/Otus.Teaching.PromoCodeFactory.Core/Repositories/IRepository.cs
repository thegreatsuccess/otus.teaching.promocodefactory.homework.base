﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Otus.Teaching.PromoCodeFactory.Core.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T?> TryGetByIdAsync(Guid id);
        Task<T> PutAsync(T entity);
        Task<bool> TryDeleteAsync(Guid id);
    }
}