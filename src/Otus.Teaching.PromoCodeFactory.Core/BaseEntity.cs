﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core
{
    public class BaseEntity
    {
        public BaseEntity(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
    }
}